import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Row, Col, Container } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  // State hooks to store the values of the input field from our user
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");

  /* We want to disable if one of the input fields is empty */
  const [isActive, setIsActive] = useState(false);

  const { user } = useContext(UserContext);

  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== ""
    ) {
      if (password1 === password2) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }
    console.log(email, password1, password2, firstName, lastName, mobileNo);
  }, [email, password1, password2, firstName, lastName, mobileNo]);
  // This function will be triggered when the inputs in the form will be submitted

  function registerUser(event) {
    event.preventDefault();
    // alert(`Congratulations! You are successfully registered using ${email}`);
    // setEmail("");
    // setPassword1("");
    // setPassword2("");
    fetch(`${process.env.REACT_APP_URI}/users/register`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        mobileNo: mobileNo,
        email: email,
        password: password1,
        verifyPassword: password2,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.email) {
          Swal.fire({
            title: "Email address already used. ",
            icon: "error",
            text: "Please use different email.",
          });
        } else if (data.mobileLength === false) {
          Swal.fire({
            title: "Invalid mobile number",
            icon: "error",
            text: "Please use an 11-digit mobile number.",
          });
        } else {
          Swal.fire({
            title: "Successfully registered!",
            icon: "success",
            text: "Welcome to Zuitt Coding Website",
          });
          <Navigate to="/" />;
        }
      });
  }
  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Container>
      <Row>
        <Col className="col-md-4 col-8 offset-md-4 offset-2">
          <Form onSubmit={registerUser} className="bg-secondary p-3">
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
              <Form.Label>Enter password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password1}
                onChange={(event) => {
                  setPassword1(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Verify your password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password2}
                onChange={(event) => {
                  setPassword2(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="First Name"
                value={firstName}
                onChange={(event) => {
                  setFirstName(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Last Name"
                value={lastName}
                onChange={(event) => {
                  setLastName(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
              <Form.Label>Mobile No.</Form.Label>
              <Form.Control
                type="number"
                placeholder="Mobile No."
                value={mobileNo}
                onChange={(event) => {
                  setMobileNo(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={!isActive}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
