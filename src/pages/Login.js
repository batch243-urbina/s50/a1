import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Row, Col, Container } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Login() {
  // State hooks to store values of the input field from the user
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  // const [user, setUser] = useState(null);

  // Allows us to consume the UserContext
  const { user, setUser } = useContext(UserContext);

  // Disable Login button when at least 1 input field is empty
  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function loginUser(event) {
    event.preventDefault();
    // alert(`You are now logged in`);

    // Set the email of the authenticated user in the local storage
    //Syntax:
    // localStorage.setItem("propertyName", value)

    // Storing information in the local storage will make the date persistent even as the page is refreshed unlike with the use of states where information is reset when refreshing the page
    // localStorage.setItem("email", email);

    // Set the global user state to have properties obtained from the local storage

    // Though access to the user information can be done via the localStorage, this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
    // setUser(localStorage.getItem("email"));
    // setEmail("");
    // setPassword("");

    // Process where in it will fetch a request to the corresponding API
    // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
    // The fetch request will communicate with our backend application providing it with a stringified JSON

    // Syntax:
    // fetch('url', {options: method, headers, body})
    // .then(response=> res.json())
    // .then(data => {data process})

    fetch(`${process.env.REACT_APP_URI}/users/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        // it is good to practice to always use or print out the result of our fetch request to ensure the correct information is received in our frontend application
        console.log(data);

        if (data.accessToken !== "empty") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login successful",
            icon: "success",
            text: "Welcome to our website!",
          });
        } else {
          Swal.fire({
            title: "Authentication failed!",
            icon: "error",
            text: "Check your login details and try again.",
          });
          setPassword("");
        }
      });

    const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_URI}/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setUser({ id: data._id, isAdmin: data.isAdmin });
        });
    };
  }

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row>
        <Col className="col-md-4 col-8 offset-md-4 offset-2">
          <h2>Login</h2>
          <Form onSubmit={loginUser}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={!isActive}>
              Login
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
