import { Link } from "react-router-dom";

export default function Error() {
  return (
    <div>
      <h1>Page Not Found</h1>
      {"Go back to the "}
      <Link to="/">homepage.</Link>
    </div>
  );
}
