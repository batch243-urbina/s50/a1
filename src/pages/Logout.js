import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Logout() {
  // Once clicked logout, it should automatically render the AppNavBar
  const { setUser, unsetUser } = useContext(UserContext);

  unsetUser();
  useEffect(() => {
    setUser({ id: null, isAdmin: false });
  }, []);

  Swal.fire({
    title: "Welcome back to the outside world!",
    icon: "info",
    text: "Babalik ka rin ",
  });

  return <Navigate to="/login" />;
}
