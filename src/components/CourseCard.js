import { Row, Col, Card, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function CourseCard({ courseProp }) {
  // destructuring the courseProp that were passed from the course.js
  const { _id, name, description, price, slots } = courseProp;

  // USE STATE HOOK
  // Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
  // States are used to keep track information related to individual components

  // Syntax:
  // const [getter, setter] = useState(initialGetterValue)

  const [enrollees, setEnrollees] = useState(0);
  const [slotsAvailable, setSlotsAvailable] = useState(slots);
  const [isAvailable, setIsAvailable] = useState(true);
  const { user } = useContext(UserContext);

  // UseEffect Hook
  // Add an "useEffect" hook to have "CourseCard" component do perform a certain task after every DOM update

  // Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

  useEffect(() => {
    if (slotsAvailable === 0) {
      setIsAvailable(false);
    }
  }, [slotsAvailable]);

  function enroll() {
    if (slotsAvailable === 1) {
      alert("Congratulations, you are the last enrollee");
    }
    setEnrollees(enrollees + 1);
    setSlotsAvailable(slotsAvailable - 1);
  }

  // Since enrollees is declared as a constant variable, directly reassigning the value is not allowed or will cause an error

  return (
    <Row>
      <Col xs={12} md={4} className="offset-md-4 offset-0">
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>
              <Card.Subtitle className="course-title-header">
                Description:
              </Card.Subtitle>
              <Card.Text className="course-title">{description}</Card.Text>
              <Card.Subtitle className="course-price-header">
                Price:
              </Card.Subtitle>
              <Card.Text className="course-price"> Php {price}</Card.Text>
              <Card.Subtitle className="course-price-header">
                Enrollees:
              </Card.Subtitle>
              <Card.Text className="course-price"> {enrollees}</Card.Text>
              <Card.Subtitle className="course-price-header">
                Slots available:
              </Card.Subtitle>
              <Card.Text className="course-price">
                {" "}
                {slotsAvailable} slots
              </Card.Text>
            </Card.Text>
            {user !== null ? (
              <Button
                as={Link}
                to={`/courses/${_id}`}
                variant="primary"
                disabled={!isAvailable}
              >
                View details
              </Button>
            ) : (
              <Button
                as={Link}
                to="/login"
                variant="primary"
                disabled={!isAvailable}
              >
                Enroll
              </Button>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
