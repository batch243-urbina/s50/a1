import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { Link } from "react-router-dom";

export default function Banner() {
  return (
    <Row className="text-center my-3">
      <Col>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere</p>
        <Button as={Link} to="/courses" variant="primary" href="/courses">
          Enroll now!
        </Button>
      </Col>
    </Row>
  );
}
