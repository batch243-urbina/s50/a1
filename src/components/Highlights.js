// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap
import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row className="my-3">
      {/* This is the 1st card */}
      <Col xs={12} md={4}>
        <Card className="card-highlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Learn From Home</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Vestibulum ut semper est, eget tempor lectus. Vivamus sed
              hendrerit justo, sit amet rutrum justo. Nam volutpat, mi sit amet
              dignissim ullamcorper, diam augue convallis enim, ac sagittis dui
              quam eu felis. Nullam orci est, maximus nec consectetur sed,
              fermentum non eros. Ut a posuere magna. Vivamus et libero id velit
              consectetur pretium. Vestibulum ante ipsum primis in faucibus orci
              luctus et ultrices posuere cubilia curae; Suspendisse vitae lorem
              sed lorem rutrum eleifend. Ut euismod ac nisl vitae commodo. In
              mauris metus, iaculis a dignissim tincidunt, cursus euismod velit.
              Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
              posuere cubilia curae; Proin cursus orci a est finibus accumsan.
              Sed varius, lacus non luctus viverra, diam urna suscipit purus, ut
              faucibus dolor massa nec risus.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      {/* This is the 2nd card */}
      <Col xs={12} md={4}>
        <Card className="card-highlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Study Now, Pay Later</h2>{" "}
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Vestibulum ut semper est, eget tempor lectus. Vivamus sed
              hendrerit justo, sit amet rutrum justo. Nam volutpat, mi sit amet
              dignissim ullamcorper, diam augue convallis enim, ac sagittis dui
              quam eu felis. Nullam orci est, maximus nec consectetur sed,
              fermentum non eros. Ut a posuere magna. Vivamus et libero id velit
              consectetur pretium. Vestibulum ante ipsum primis in faucibus orci
              luctus et ultrices posuere cubilia curae; Suspendisse vitae lorem
              sed lorem rutrum eleifend. Ut euismod ac nisl vitae commodo. In
              mauris metus, iaculis a dignissim tincidunt, cursus euismod velit.
              Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
              posuere cubilia curae; Proin cursus orci a est finibus accumsan.
              Sed varius, lacus non luctus viverra, diam urna suscipit purus, ut
              faucibus dolor massa nec risus.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      {/* This is the 3rd card */}
      <Col xs={12} md={4}>
        <Card className="card-highlight p-3">
          <Card.Body>
            <Card.Title>
              <h2> Learn From Home</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Vestibulum ut semper est, eget tempor lectus. Vivamus sed
              hendrerit justo, sit amet rutrum justo. Nam volutpat, mi sit amet
              dignissim ullamcorper, diam augue convallis enim, ac sagittis dui
              quam eu felis. Nullam orci est, maximus nec consectetur sed,
              fermentum non eros. Ut a posuere magna. Vivamus et libero id velit
              consectetur pretium. Vestibulum ante ipsum primis in faucibus orci
              luctus et ultrices posuere cubilia curae; Suspendisse vitae lorem
              sed lorem rutrum eleifend. Ut euismod ac nisl vitae commodo. In
              mauris metus, iaculis a dignissim tincidunt, cursus euismod velit.
              Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
              posuere cubilia curae; Proin cursus orci a est finibus accumsan.
              Sed varius, lacus non luctus viverra, diam urna suscipit purus, ut
              faucibus dolor massa nec risus.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
